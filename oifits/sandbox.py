from oivis.oimisc import mcfost_load
from oivis.oifourier import imavis
from oivis.oifits import oifits_read2
from oivis.oifits import oifits_transform as oitrans
import importlib
from astropy.io import fits
import glob as glob

importlib.reload(mcfost_load)
importlib.reload(oifits_read2)
importlib.reload(oitrans)
importlib.reload(imavis)

cmap = matplotlib.cm.get_cmap('Spectral')

atdatafilelist = glob.glob("/Users/bergejea/Nextcloud/Projets/HD100546/Donnees/GravityGTO/AT/*fits")
utdatafilelist = glob.glob("/Users/bergejea/Nextcloud/Projets/HD100546/Donnees/GravityGTO/UT/*fits")


# AT data
atoid = oifits_read2.Oifits(atdatafilelist,loglevel="ERROR")
atdatarawblocks = atoid.oidata

#UT data
utoid = oifits_read2.Oifits(utdatafilelist,loglevel="ERROR")
utdatarawblocks = utoid.oidata

# Separating FT and SC and Removing weird vis2 points (0<< & >>1)
atftdatablocks = oitrans.filter_by_keyword(atdatarawblocks,"INSNAME","GRAVITY_FT")
atscdatablocks = oitrans.filter_by_keyword(atdatarawblocks,"INSNAME","GRAVITY_SC")
utftdatablocks = oitrans.filter_by_keyword(utdatarawblocks,"INSNAME","GRAVITY_FT")
utscdatablocks = oitrans.filter_by_keyword(utdatarawblocks,"INSNAME","GRAVITY_SC")
atftvisdatablocks = oitrans.filter_by_keyword(atftdatablocks,"TYPE","V2")
atscvisdatablocks = oitrans.filter_by_keyword(atscdatablocks,"TYPE","V2")
utftvisdatablocks = oitrans.filter_by_keyword(utftdatablocks,"TYPE","V2")
utscvisdatablocks = oitrans.filter_by_keyword(utscdatablocks,"TYPE","V2")
# Cleaning vis2 data
#atftvisdatablocks = oitrans.filter_by_keyword_range(atftvis2datablocks,"VISDATA", [0.00,1.5])
#atscvisdatablocks = oitrans.filter_by_keyword_range(atscvis2datablocks,"VISDATA", [0.01,1.5])

