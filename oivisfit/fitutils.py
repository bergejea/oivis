# useful functions for handling visibility functions
import numpy as np
import sys
thismod = sys.modules[__name__]


def radial_2d_grid(radius, sample, center=[0, 0]):

    x = np.linspace(-radius, radius, sample) + center[1]
    y = np.linspace(-radius, radius, sample) + center[0]
    xi1 = np.tile(x, sample)
    xx = xi1.reshape(sample, sample)
    yi1 = np.repeat(y, sample)
    yy = yi1.reshape(sample, sample)
    radgrid = np.sqrt(xx**2+yy**2)
    return xi1, yi1, radgrid

def vis_grid(bmax, gridim, wavel, params, funcname):
    """
    visgrid = vis_grid(bmax, gridim, wavel, params, funcname):

    Computes an image of the Fourier space visibility and direct
    space image on a grid of maximum spatial frequency bmax/wavel and
    numerical size gridim. 
    
    Parameters
    ----------
    
    bmax : float (meters)
        Maximum baseline used in the grid
    gridim : integer
        Number of cells in the (radial) grid
    wavel: float (meters)
        Wavelength of observation
    params : dictionary 
        parameters to define the visibility function
    funcname : function call (from functions.py)
        Visibility function to visualise (e.g vis_sinusoidal_ring)
    
    Returns
    -------
    
    visgrid['xi1'] : Radial grid in the x direction

    visgrid['yi1'] : Radial grid in the y direction

    visgrid['visibility'] : Complex array of dim gridim x gridim containing
        the visibility of funcname with parameters param

    visgrid['image'] = Array of dim gridim x gridm containing the
        fourier transform of the visibility.

     An example::
        visgrid = visfuncs.vis_grid(250, 250, 1.65e-6, params, combine_functions)

    """
    # computing the radial grid
    # xi1 and yi1 are the spatial frequencies grid for each direction
    xi1, yi1, radgrid = radial_2d_grid(bmax, gridim)
    # converting function name string into a real function
    #JPB visfunc = getattr(thismod, funcname)
    # spatial frequency array
    x = [xi1, yi1, wavel, wavel]
    # computing the visibility
    # JPB vis = visfunc(x, params)
    vis = funcname(x, params)
    # reshaping and ft to get the proper ft and direct image of the
    # objects
    visgrid = vis['VIS'].reshape(gridim, gridim)
    tfvis = np.fft.fft2(visgrid)
    modtf = np.fft.fftshift(np.absolute(tfvis))
    modima = np.transpose(modtf[::-1, ::]) #FIXME: not sure this is according to convention
    #TODO: see paper by fedele 2021 alma image with proper orientation conventions
    visimage = {}
    visimage['xi1'] = xi1
    visimage['yi1'] = yi1
    visimage['VIS'] = visgrid
    visimage['IMAGE'] = modima

    return visimage