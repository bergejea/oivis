# oivis

## goals
Tools useful for optical interferometry:
- simulating interferometric data
- manipulating oifits files
- fitting interferometric data
- photometry
- simulating instruments

Crappy coding. Use at your own risks

## To be done

- in oifits_read: remove get_v2data and get_cpdata from Oifits
- rajouter les frequence spatiale dans get_v2data, get_t3data
- warning: with new agnostic eration position angle needs to be defined with care. Needs message to inform in cas eratio < 1 it will be reversed
- include BB functions in the functions notebook
- robustness to absence of visibility/cp